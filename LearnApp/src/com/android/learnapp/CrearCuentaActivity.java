package com.android.learnapp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.learnapp.db.UsersDataSource;
import com.android.learnapp.model.User;
import com.android.learnapp.utils.UIHelper;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CrearCuentaActivity extends Activity{

	Spinner spinner;
	String[] opciones = {"Grado escolar", "Kinder", "1ro - 3ro de Primaria", "4to - 6to de Primaria", "Secundaria", "Otro"};
	String user="";
	String pass="";
	String grado="";
	String gen="N";
	long id;


	//DB
	UsersDataSource datasource;
	private static final String LOGTAG = "APPRENDE";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.crear_cuenta_layout);
		getActionBar().setDisplayHomeAsUpEnabled(true);


		spinner = (Spinner)findViewById(R.id.spinnerGrado);
		ArrayAdapter<String> adaptadorSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item ,opciones);
		spinner.setAdapter(adaptadorSpinner);

		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				switch (i) {
				case 0:	
					grado="";
					break;
				case 1:
					//Toast.makeText(getApplicationContext(), opciones[i], Toast.LENGTH_LONG).show();	
					grado=opciones[i];
					break;
				case 2:
					//Toast.makeText(getApplicationContext(), opciones[i], Toast.LENGTH_LONG).show();
					grado=opciones[i];
					break;
				case 3:
					//Toast.makeText(getApplicationContext(), opciones[i], Toast.LENGTH_LONG).show();
					grado=opciones[i];
					break;
				case 4:
					//Toast.makeText(getApplicationContext(), opciones[i], Toast.LENGTH_LONG).show();
					grado=opciones[i];
					break;
				case 5:
					//Toast.makeText(getApplicationContext(), opciones[i], Toast.LENGTH_LONG).show();
					grado=opciones[i];
					break;
				}

			}



			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {	
			}			
		});

		ImageView nino = (ImageView) findViewById(R.id.imageViewNino);
		nino.setClickable(true);
		ImageView nina = (ImageView) findViewById(R.id.imageViewNina);
		nina.setClickable(true);
		nino.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gen="M";
				v.setBackgroundColor(Color.GREEN);
				ImageView nina = (ImageView) findViewById(R.id.imageViewNina);
				nina.setBackgroundColor(0);
			}
		});
		nina.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gen="F";
				v.setBackgroundColor(Color.GREEN);
				ImageView nino = (ImageView) findViewById(R.id.imageViewNino);
				nino.setBackgroundColor(0);
			}
		});


		Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/Chalkduster.ttf");
		TextView tv = (TextView) findViewById(R.id.textViewCrearCuenta);
		tv.setTypeface(tf);   
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home){
			finish();		
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.crear_menu, menu);
		return true;
	}

	public void goToMapa(View v ) throws IOException, JSONException{				

		/*JSONArray datos= new JSONArray();
		JSONObject usuario = new JSONObject();*/

		user=UIHelper.getText(this, R.id.editTextUsuario);
		pass=UIHelper.getText(this, R.id.editTextPassword);

		if((!user.equals("")) && (!pass.equals("")) && (!gen.equals("N")) && (!grado.equals(""))){
			/*usuario.put("Usuario",user);
			usuario.put("Password",pass);
			usuario.put("Genero",gen);
			usuario.put("Grado",grado);
			datos.put(usuario);

			String text=datos.toString();
			FileOutputStream fos=openFileOutput("usuarios", MODE_PRIVATE);
			fos.write(text.getBytes());
			fos.close();*/


			//DB
			datasource = new UsersDataSource(this);
			datasource.open();
			createData();
			datasource.close();

			Intent intent = new Intent(this, MapaActivity.class);
			intent.putExtra("idUsuario",id);
			startActivity(intent);
			finish();

		}
		else{
			Toast.makeText(getApplicationContext(), "�Llena todos los campos!", Toast.LENGTH_LONG).show();		
		}	
		//UIHelper.displayText(this,R.id.textViewPassword,"File written to disk:\n" + datos.toString());	
	}
	/*@Override
	protected void onResume() {
		super.onResume();
		datasource.open();
	}

	@Override
	protected void onPause() {
		super.onPause();
		datasource.close();
	}*/

	private void createData(){
		User usuario = new User();
		usuario.setUser(user);
		usuario.setPassword(pass);
		usuario.setGender(gen);
		usuario.setGrade(grado);
		usuario.setPoints(0);
		usuario = datasource.create(usuario);
		Log.i(LOGTAG, "id "+usuario.getId());
		id=usuario.getId();


		/*UsersPullParser parser= new UsersPullParser();
		List<User> users=parser.parseXML(this);

		for (User user:users){
			datasource.create(user);
		}*/

	}



}
