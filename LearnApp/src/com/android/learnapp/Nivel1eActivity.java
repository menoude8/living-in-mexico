package com.android.learnapp;

import com.android.learnapp.db.UsersDBOpenHelper;
import com.android.learnapp.db.UsersDataSource;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Nivel1eActivity extends Activity{

	Spinner sBenito, sHugo, sSor, sFrida;
	Boolean rBenito = false;
	Boolean rHugo = false;
	Boolean rSor = false;
	Boolean rFrida = false;
	String[] opciones = {" ", "Hugo S�nchez", "Benito Ju�rez", "Sor Juana In�s", "Frida Kahlo"};

	boolean wrong=false;
	String score="";
	
	long id;
	UsersDataSource datasource;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel1e_layout);  

		Intent intent = getIntent();
		score = intent.getStringExtra("score");
		id=intent.getLongExtra("idUsuario",0);
		
		((TextView)findViewById(R.id.textViewScore)).setText(score);
		final TextView reloj = (TextView) findViewById(R.id.textViewReloj);

		Thread t = new Thread() {

			@Override
			public void run() {
				try {
					while (!isInterrupted()) {
						Thread.sleep(1000);
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								int time=Integer.parseInt((String)reloj.getText());
								String newT;
								if(wrong){
									newT= String.valueOf(time+11);
									wrong=false;
								}
								else{
									newT= String.valueOf(time+1);
								}
								score=newT;
								reloj.setText(newT);
							}
						});
					}
				} catch (InterruptedException e) {
				}
			}
		};

		t.start();


		sBenito = (Spinner)findViewById(R.id.spinnerBenito);
		ArrayAdapter<String> adaptadorSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item ,opciones);
		sBenito.setAdapter(adaptadorSpinner);
		sBenito.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				switch (i) {
				case 0:	
					rBenito = false;
					break;
				case 1:
					rBenito = false;
					break;
				case 2:
					rBenito = true;
					break;
				case 3:
					rBenito = false;
					break;
				case 4:
					rBenito = false;
					break;
				}	
			}
			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {	
			}			
		});
		sHugo = (Spinner)findViewById(R.id.spinnerHugo);
		ArrayAdapter<String> adaptadorSpinnerH = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item ,opciones);
		sHugo.setAdapter(adaptadorSpinnerH);
		sHugo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				switch (i) {
				case 0:	
					rHugo = false;
					break;
				case 1:
					rHugo = true;
					break;
				case 2:
					rHugo = false;
					break;
				case 3:
					rHugo = false;
					break;
				case 4:
					rHugo = false;
					break;
				}	
			}
			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {	
			}			
		});
		sFrida = (Spinner)findViewById(R.id.spinnerFrida);
		ArrayAdapter<String> adaptadorSpinnerF = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item ,opciones);
		sFrida.setAdapter(adaptadorSpinnerF);
		sFrida.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				switch (i) {
				case 0:	
					rFrida = false;
					break;
				case 1:
					rFrida = false;
					break;
				case 2:
					rFrida = false;
					break;
				case 3:
					rFrida = false;
					break;
				case 4:
					rFrida = true;
					break;
				}	
			}
			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {	
			}			
		});
		sSor = (Spinner)findViewById(R.id.spinnerSor);
		ArrayAdapter<String> adaptadorSpinnerS = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item ,opciones);
		sSor.setAdapter(adaptadorSpinnerS);
		sSor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				switch (i) {
				case 0:	
					rSor = false;
					break;
				case 1:
					rSor = false;
					break;
				case 2:
					rSor = false;
					break;
				case 3:
					rSor = true;
					break;
				case 4:
					rSor = false;
					break;
				}	
			}
			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {	
			}			
		});      
	}

	public void nextLevel(View v){
		if(rSor && rFrida && rHugo && rBenito){
			Intent intent = new Intent(this, Nivel1fActivity.class);
			int oldScore=Integer.parseInt((String)((TextView)findViewById(R.id.textViewScore)).getText());
			int levelScore=Integer.parseInt(score);
			String newScore=String.valueOf(levelScore+oldScore);
			//DB
			ContentValues updatedValues = new ContentValues();
	        updatedValues.put(UsersDBOpenHelper.COLUMN_POINTS, newScore);
	        String where = UsersDBOpenHelper.COLUMN_ID + "=" + id;
	        datasource = new UsersDataSource(this);
	        datasource.open();
	        datasource.getDatabase().update(UsersDBOpenHelper.TABLE_USERS, updatedValues, where, null);
	        
	        Cursor cursor= datasource.getDatabase().query(UsersDBOpenHelper.TABLE_USERS, UsersDataSource.getAllcolumns(), null, null, null, null, null);
	        long idS;
		if(cursor.getCount()>0){
			while(cursor.moveToNext()){
				idS=cursor.getLong(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_ID));
				if(idS==id){
					String tmp=cursor.getString(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_POINTS));
					Toast.makeText(getApplicationContext(), tmp, Toast.LENGTH_LONG).show();

				}
			}
		}
	        
			intent.putExtra("score",newScore);
			intent.putExtra("idUsuario",id);
			
			startActivity(intent);
			finish();
		}
		else{
			wrong=true;
			Toast.makeText(getApplicationContext(), "Incorrecto", Toast.LENGTH_LONG).show();	
		}    
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}


}
