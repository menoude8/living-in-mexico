package com.android.learnapp;

import com.android.learnapp.db.UsersDBOpenHelper;
import com.android.learnapp.db.UsersDataSource;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Nivel1kActivity extends Activity {
	
	boolean wrong=false;
	String score="";
	
	long id;
	UsersDataSource datasource;
	
	Spinner sFox, sFelipe, sEnrique;
	Boolean rFox= false;
	Boolean rFelipe=false;
	Boolean rEnrique=false;
	String[] opciones = {" ", "2012-presente", "2006-2012", "2000-2006"};
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel1k_layout);
		
		Intent intent = getIntent();      
        score = intent.getStringExtra("score");
        id=intent.getLongExtra("idUsuario",0);
        ((TextView)findViewById(R.id.textViewScore)).setText(score);
        final TextView reloj = (TextView) findViewById(R.id.textViewReloj);

		Thread t = new Thread() {

			@Override
			public void run() {
				try {
					while (!isInterrupted()) {
						Thread.sleep(1000);
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								int time=Integer.parseInt((String)reloj.getText());
								String newT;
								if(wrong){
									newT= String.valueOf(time+11);
									wrong=false;
								}
								else{
									newT= String.valueOf(time+1);
								}
								score=newT;
								reloj.setText(newT);
							}
						});
					}
				} catch (InterruptedException e) {
				}
			}
		};

		t.start();
		
		sEnrique = (Spinner)findViewById(R.id.spinnerEnrique);
		ArrayAdapter<String> adaptadorSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item ,opciones);
		sEnrique.setAdapter(adaptadorSpinner);
		sEnrique.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				switch (i) {
				case 0:	
					rEnrique = false;
					break;
				case 1:
					rEnrique = true;
					break;
				case 2:
					rEnrique = false;
					break;
				case 3:
					rEnrique = false;
					break;
				}	
			}
			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {	
			}			
		});

		
		sFelipe = (Spinner)findViewById(R.id.spinnerFelipe);
		ArrayAdapter<String> adaptadorSpinner2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item ,opciones);
		sFelipe.setAdapter(adaptadorSpinner2);
		sFelipe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				switch (i) {
				case 0:	
					rFelipe = false;
					break;
				case 1:
					rFelipe = false;
					break;
				case 2:
					rFelipe = true;
					break;
				case 3:
					rFelipe = false;
					break;
				}	
			}
			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {	
			}			
		});

		
		sFox = (Spinner)findViewById(R.id.spinnerFox);
		ArrayAdapter<String> adaptadorSpinner3 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item ,opciones);
		sFox.setAdapter(adaptadorSpinner3);
		sFox.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				switch (i) {
				case 0:	
					rFox = false;
					break;
				case 1:
					rFox = false;
					break;
				case 2:
					rFox = false;
					break;
				case 3:
					rFox = true;
					break;
				}	
			}
			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {	
			}			
		});

	
	}

	public void nextLevel(View v){
		if(rFelipe && rFox && rEnrique){
			Intent intent = new Intent(this, Nivel1lActivity.class);
			int oldScore=Integer.parseInt((String)((TextView)findViewById(R.id.textViewScore)).getText());
			int levelScore=Integer.parseInt(score);
			String newScore=String.valueOf(levelScore+oldScore);
			//DB
			ContentValues updatedValues = new ContentValues();
	        updatedValues.put(UsersDBOpenHelper.COLUMN_POINTS, newScore);
	        String where = UsersDBOpenHelper.COLUMN_ID + "=" + id;
	        datasource = new UsersDataSource(this);
	        datasource.open();
	        datasource.getDatabase().update(UsersDBOpenHelper.TABLE_USERS, updatedValues, where, null);
	        
	        Cursor cursor= datasource.getDatabase().query(UsersDBOpenHelper.TABLE_USERS, UsersDataSource.getAllcolumns(), null, null, null, null, null);
	        long idS;
		if(cursor.getCount()>0){
			while(cursor.moveToNext()){
				idS=cursor.getLong(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_ID));
				if(idS==id){
					String tmp=cursor.getString(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_POINTS));
					Toast.makeText(getApplicationContext(), tmp, Toast.LENGTH_LONG).show();

				}
			}
		}
	        
			intent.putExtra("score",newScore);
			intent.putExtra("idUsuario",id);
			
			startActivity(intent);
			finish();
		}
		else{
			wrong=true;
			Toast.makeText(getApplicationContext(), "Incorrecto", Toast.LENGTH_LONG).show();	
		}    
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.crear_menu, menu);
		return true;
	}

}


