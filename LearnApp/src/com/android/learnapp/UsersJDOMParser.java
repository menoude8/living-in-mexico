package com.android.learnapp;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import android.content.Context;
import android.util.Log;

import com.android.learnapp.model.User;

public class UsersJDOMParser {

	private static final String LOGTAG = "APPRENDE";

	private static final String USER_TAG = "user";
	private static final String USER_ID = "userId";
	private static final String USER_USER = "userUser";
	private static final String USER_PASSWORD = "password";
	private static final String USER_GENDER = "gender";
	private static final String USER_GRADE ="grade";
	private static final String USER_POINTS ="points";
	

	public List<User> parseXML(Context context) {

		InputStream stream = context.getResources().openRawResource(R.raw.users);
		SAXBuilder builder = new SAXBuilder();
		List<User> tours = new ArrayList<User>();

		try {

			Document document = (Document) builder.build(stream);
			org.jdom2.Element rootNode = document.getRootElement();
			List<org.jdom2.Element> list = rootNode.getChildren(USER_TAG);

			for (Element node : list) {
				User user = new User();
				user.setId(Integer.parseInt(node.getChildText(USER_ID)));
				user.setUser(node.getChildText(USER_USER));
				user.setPassword(node.getChildText(USER_PASSWORD));
				user.setGender(USER_GENDER);
				user.setGrade(node.getChildText(USER_GRADE));
				user.setPoints(Integer.parseInt(node.getChildText(USER_POINTS)));
				tours.add(user);
			}

		} catch (IOException e) {
			Log.i(LOGTAG, e.getMessage());
		} catch (JDOMException e) {
			Log.i(LOGTAG, e.getMessage());
		}
		return tours;
	}

}
