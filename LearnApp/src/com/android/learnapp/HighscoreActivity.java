package com.android.learnapp;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.learnapp.db.UsersDBOpenHelper;
import com.android.learnapp.db.UsersDataSource;
import com.android.learnapp.model.*;

public class HighscoreActivity extends ListActivity {

	//DB
	UsersDataSource datasource;
	private static final String LOGTAG = "APPRENDE";
	private List<User> users;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.highscore_layout);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		datasource = new UsersDataSource(this);
		datasource.open();
		users = datasource.findPoints();
		ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, 
				android.R.layout.simple_list_item_1, users);
		setListAdapter(adapter);

		Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/Chalkduster.ttf");
		TextView tv = (TextView) findViewById(R.id.textViewScore);
		tv.setTypeface(tf);   
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home){
			finish();		
		}
		else{
			Intent intent;
			switch (item.getItemId()) {
			case R.id.action_acerca:
				intent = new Intent(this, AcercaActivity.class);
				startActivity(intent);
				break;
			case R.id.action_puntos:
				intent = new Intent(this, HighscoreActivity.class);
				startActivity(intent);
				break;
			case R.id.action_sms:
				intent = new Intent(this, SendSMSActivity.class);
				startActivity(intent);
				break;

			}
		}
		return super.onOptionsItemSelected(item);
	}

}

