package com.android.learnapp;

import com.android.learnapp.db.UsersDBOpenHelper;
import com.android.learnapp.db.UsersDataSource;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class Nivel1gActivity extends Activity{

	boolean wrong=false;
	String score="";
	
	long id;
	UsersDataSource datasource;


	private RadioButton button1_2 = null;
	private RadioButton button2_1 = null;
	private RadioButton button3_2 = null;
	private RadioButton button4_1 = null;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel1g_layout);

		Intent intent = getIntent();
		score = intent.getStringExtra("score");
		id=intent.getLongExtra("idUsuario",0);
		((TextView)findViewById(R.id.textViewScore)).setText(score);
		final TextView reloj = (TextView) findViewById(R.id.textViewReloj);

		Thread t = new Thread() {

			@Override
			public void run() {
				try {
					while (!isInterrupted()) {
						Thread.sleep(1000);
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								int time=Integer.parseInt((String)reloj.getText());
								String newT;
								if(wrong){
									newT= String.valueOf(time+11);
									wrong=false;
								}
								else{
									newT= String.valueOf(time+1);
								}
								score=newT;
								reloj.setText(newT);
							}
						});
					}
				} catch (InterruptedException e) {
				}
			}
		};

		t.start();

		button1_2 = (RadioButton) findViewById(R.id.radioButton1_2);
		button2_1 = (RadioButton) findViewById(R.id.radioButton2_1);
		button3_2 = (RadioButton) findViewById(R.id.radioButton3_2);
		button4_1 = (RadioButton) findViewById(R.id.radioButton4_1);
	}



	public void nextLevel(View v){
		if(button1_2.isChecked() && button2_1.isChecked() && button3_2.isChecked() && button4_1.isChecked() ){
			Intent intent = new Intent(this, Nivel1hActivity.class);
			int oldScore=Integer.parseInt((String)((TextView)findViewById(R.id.textViewScore)).getText());
			int levelScore=Integer.parseInt(score);
			String newScore=String.valueOf(levelScore+oldScore);
			//DB
			ContentValues updatedValues = new ContentValues();
	        updatedValues.put(UsersDBOpenHelper.COLUMN_POINTS, newScore);
	        String where = UsersDBOpenHelper.COLUMN_ID + "=" + id;
	        datasource = new UsersDataSource(this);
	        datasource.open();
	        datasource.getDatabase().update(UsersDBOpenHelper.TABLE_USERS, updatedValues, where, null);
	        
	        Cursor cursor= datasource.getDatabase().query(UsersDBOpenHelper.TABLE_USERS, UsersDataSource.getAllcolumns(), null, null, null, null, null);
	        long idS;
		if(cursor.getCount()>0){
			while(cursor.moveToNext()){
				idS=cursor.getLong(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_ID));
				if(idS==id){
					String tmp=cursor.getString(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_POINTS));
					Toast.makeText(getApplicationContext(), tmp, Toast.LENGTH_LONG).show();

				}
			}
		}
	        
			intent.putExtra("score",newScore);
			intent.putExtra("idUsuario",id);
			
			startActivity(intent);
			finish();
		}
		else{
			wrong=true;
			Toast.makeText(getApplicationContext(), "Incorrecto", Toast.LENGTH_LONG).show();
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

}
