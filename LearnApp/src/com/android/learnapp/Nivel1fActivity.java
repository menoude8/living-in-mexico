package com.android.learnapp;

import com.android.learnapp.db.UsersDBOpenHelper;
import com.android.learnapp.db.UsersDataSource;

import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;

public class Nivel1fActivity extends Activity implements OnTouchListener, OnDragListener {

	boolean wrong=false;
	String score="";

	long id;
	UsersDataSource datasource;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nivel1f_layout);

		Intent intent = getIntent();
		score = intent.getStringExtra("score");
		id=intent.getLongExtra("idUsuario",0);

		((TextView)findViewById(R.id.textViewScore)).setText(score);
		final TextView reloj = (TextView) findViewById(R.id.textViewReloj);

		Thread t = new Thread() {

			@Override
			public void run() {
				try {
					while (!isInterrupted()) {
						Thread.sleep(1000);
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								int time=Integer.parseInt((String)reloj.getText());
								String newT;
								if(wrong){
									newT= String.valueOf(time+11);
									wrong=false;
								}
								else{
									newT= String.valueOf(time+1);
								}
								score=newT;
								reloj.setText(newT);
							}
						});
					}
				} catch (InterruptedException e) {
				}
			}
		};

		t.start();

		findViewById(R.id.top_container).setOnDragListener(this);
		findViewById(R.id.bottom_container).setOnDragListener(this);
		findViewById(R.id.textView1).setOnTouchListener(this);
		findViewById(R.id.textView2).setOnTouchListener(this);
		findViewById(R.id.textView3).setOnTouchListener(this);
		findViewById(R.id.textView4).setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent e) {
		if (e.getAction() == MotionEvent.ACTION_DOWN) {
			DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
			v.startDrag(null, shadowBuilder, v, 0);
			v.setVisibility(View.INVISIBLE);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean onDrag(View v, DragEvent e) {
		View view = (View) e.getLocalState();
		if (e.getAction()==DragEvent.ACTION_DROP) {
			ViewGroup from = (ViewGroup) view.getParent();
			from.removeView(view);
			LinearLayout to = (LinearLayout) v;
			to.addView(view);
			view.setVisibility(View.VISIBLE);
		}
		if (e.getAction()==DragEvent.ACTION_DRAG_ENDED) {
			if(!e.getResult()){
				view.setVisibility(View.VISIBLE);
			}

		}

		return true;
	}
	public void revisar(View v){
		LinearLayout lay = (LinearLayout) findViewById(R.id.bottom_container);
		if(lay.findViewById(R.id.textView3) != null && lay.getChildCount()==1){
			Toast.makeText(this, "�Correcto!", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(this, Nivel1gActivity.class);
			int oldScore=Integer.parseInt((String)((TextView)findViewById(R.id.textViewScore)).getText());
			int levelScore=Integer.parseInt(score);
			String newScore=String.valueOf(levelScore+oldScore);
			//DB
			ContentValues updatedValues = new ContentValues();
	        updatedValues.put(UsersDBOpenHelper.COLUMN_POINTS, newScore);
	        String where = UsersDBOpenHelper.COLUMN_ID + "=" + id;
	        datasource = new UsersDataSource(this);
	        datasource.open();
	        datasource.getDatabase().update(UsersDBOpenHelper.TABLE_USERS, updatedValues, where, null);
	        
	        Cursor cursor= datasource.getDatabase().query(UsersDBOpenHelper.TABLE_USERS, UsersDataSource.getAllcolumns(), null, null, null, null, null);
	        long idS;
		if(cursor.getCount()>0){
			while(cursor.moveToNext()){
				idS=cursor.getLong(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_ID));
				if(idS==id){
					String tmp=cursor.getString(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_POINTS));
					Toast.makeText(getApplicationContext(), tmp, Toast.LENGTH_LONG).show();

				}
			}
		}
	        
			intent.putExtra("score",newScore);
			intent.putExtra("idUsuario",id);
			
			startActivity(intent);
			finish();
		}
		else{
			wrong=true;
			Toast.makeText(this, "�Incorrecto!", Toast.LENGTH_SHORT).show();
		}

	}

}
