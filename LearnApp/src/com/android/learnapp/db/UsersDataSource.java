package com.android.learnapp.db;

import java.util.ArrayList;
import java.util.List;

import com.android.learnapp.model.User;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class UsersDataSource {

	public static final String LOGTAG="APPRENDE";
	
	SQLiteOpenHelper dbhelper;
	private SQLiteDatabase database;
	
	
	private static final String[] allColumns = {
		UsersDBOpenHelper.COLUMN_ID,
		UsersDBOpenHelper.COLUMN_USER,
		UsersDBOpenHelper.COLUMN_PASSWORD,
		UsersDBOpenHelper.COLUMN_GENDER,
		UsersDBOpenHelper.COLUMN_GRADE,
		UsersDBOpenHelper.COLUMN_POINTS};
	
	public static String[] getAllcolumns() {
		return allColumns;
	}

	public UsersDataSource(Context context) {
		dbhelper = new UsersDBOpenHelper(context);
	}
	
	public void open() {
		Log.i(LOGTAG, "Database opened");
		setDatabase(dbhelper.getWritableDatabase());
	}

	public void close() {
		Log.i(LOGTAG, "Database closed");		
		dbhelper.close();
	}
	
	public User create(User user) {
		ContentValues values = new ContentValues();
		values.put(UsersDBOpenHelper.COLUMN_USER, user.getUser());
		values.put(UsersDBOpenHelper.COLUMN_PASSWORD, user.getPassword());
		values.put(UsersDBOpenHelper.COLUMN_GENDER, user.getGender());
		values.put(UsersDBOpenHelper.COLUMN_GRADE, user.getGrade());
		values.put(UsersDBOpenHelper.COLUMN_POINTS, user.getPoints());
		long insertid = getDatabase().insert(UsersDBOpenHelper.TABLE_USERS, null, values);
		user.setId(insertid);
		return user;
	}
	
	
public List<User> findAll() {
		
		Cursor cursor = database.query(UsersDBOpenHelper.TABLE_USERS, allColumns, 
				null, null, null, null, null);
				
		Log.i(LOGTAG, "Returned " + cursor.getCount() + " rows");
		List<User> users = cursorToList(cursor);
		return users;
	}

	public List<User> findPoints() {
		String where= UsersDBOpenHelper.COLUMN_POINTS + "<>0";
		Cursor cursor = database.query(UsersDBOpenHelper.TABLE_USERS, allColumns, 
				where, null, null, null, UsersDBOpenHelper.COLUMN_POINTS);
		
		Log.i(LOGTAG, "Returned " + cursor.getCount() + " rows");
		List<User> users = cursorToList(cursor);
		return users;
	}
	
	private List<User> cursorToList(Cursor cursor) {
		List<User> users = new ArrayList<User>();
		if (cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				User user=new User();
				user.setId(cursor.getLong(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_ID)));
				user.setUser(cursor.getString(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_USER)));
				user.setPassword(cursor.getString(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_PASSWORD)));
				user.setGender(cursor.getString(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_GENDER)));
				user.setGrade(cursor.getString(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_GRADE)));
				user.setPoints(cursor.getInt(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_POINTS)));
				users.add(user);
			}
		}
		return users;
	}

	public SQLiteDatabase getDatabase() {
		return database;
	}

	public void setDatabase(SQLiteDatabase database) {
		this.database = database;
	}
	
	
	
}
