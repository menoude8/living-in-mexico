package com.android.learnapp.model;

import java.text.NumberFormat;


public class User {
	private long id;
	private String user;
	private String password;
	private String gender;
	private String grade;
	private int points;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public double getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public String toString() {
		//NumberFormat nf = NumberFormat.getCurrencyInstance();
		return "              " + user + "                     " + points;
	}
}
