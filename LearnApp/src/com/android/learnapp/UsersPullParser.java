package com.android.learnapp;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.util.Log;

import com.android.learnapp.model.User;

public class UsersPullParser {

	private static final String LOGTAG = "APPRENDE";
	
	private static final String USER_ID = "tourId";
	private static final String USER_USER = "user";
	private static final String USER_PASSWORD = "password";
	private static final String USER_GENDER = "gender";
	private static final String USER_GRADE = "grade";
	private static final String USER_POINTS = "points";
	
	private User currentUser  = null;
	private String currentTag = null;
	List<User> users = new ArrayList<User>();

	public List<User> parseXML(Context context) {

		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			
			InputStream stream = context.getResources().openRawResource(R.raw.users);
			xpp.setInput(stream, null);

			int eventType = xpp.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				if (eventType == XmlPullParser.START_TAG) {
					handleStartTag(xpp.getName());
				} else if (eventType == XmlPullParser.END_TAG) {
					currentTag = null;
				} else if (eventType == XmlPullParser.TEXT) {
					handleText(xpp.getText());
				}
				eventType = xpp.next();
			}

		} catch (NotFoundException e) {
			Log.d(LOGTAG, e.getMessage());
		} catch (XmlPullParserException e) {
			Log.d(LOGTAG, e.getMessage());
		} catch (IOException e) {
			Log.d(LOGTAG, e.getMessage());
		}

		return users;
	}

	private void handleText(String text) {
		String xmlText = text;
		if (currentUser != null && currentTag != null) {
			if (currentTag.equals(USER_ID)) {
				Integer id = Integer.parseInt(xmlText);
				currentUser.setId(id);
			} 
			else if (currentTag.equals(USER_USER)) {
				currentUser.setUser(xmlText);
			}
			else if (currentTag.equals(USER_PASSWORD)) {
				currentUser.setPassword(xmlText);
			}
			else if (currentTag.equals(USER_GRADE)) {
				currentUser.setGrade(xmlText);
			}
			else if (currentTag.equals(USER_GENDER)) {
				currentUser.setGender(xmlText);
			}
			else if (currentTag.equals(USER_POINTS)) {
				int points = Integer.parseInt(xmlText);
				currentUser.setPoints(points);
			}
		}
	}

	private void handleStartTag(String name) {
		if (name.equals("user")) {
			currentUser = new User();
			users.add(currentUser);
		}
		else {
			currentTag = name;
		}
	}
}
