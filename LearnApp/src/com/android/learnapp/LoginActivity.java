package com.android.learnapp;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.android.learnapp.db.UsersDBOpenHelper;
import com.android.learnapp.db.UsersDataSource;
import com.android.learnapp.model.User;
import com.android.learnapp.utils.UIHelper;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;


public class LoginActivity extends Activity {

	public static final String LOGTAG="APPRENDE";
	
	//DB
	public static final String USERNAME="username";
	private static final String[] allColumns = {
		UsersDBOpenHelper.COLUMN_ID,
		UsersDBOpenHelper.COLUMN_USER,
		UsersDBOpenHelper.COLUMN_PASSWORD,
		UsersDBOpenHelper.COLUMN_GENDER,
		UsersDBOpenHelper.COLUMN_GRADE,
		UsersDBOpenHelper.COLUMN_POINTS};

	boolean status=false;
	UsersDataSource datasource;
	long id;
	
	MediaPlayer mp1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);
		getActionBar().hide();
		mp1 = MediaPlayer.create(LoginActivity.this, R.raw.jmin);
		mp1.start();

	}

	//Te lleva a Mapa
	public void goToMapa(View v)throws IOException, JSONException{
		/*File file = getBaseContext().getFileStreamPath("usuarios");
    	if(file.exists()){
    		Log.d("LoginActivity", "Existe el archivo");
    		FileInputStream fis=openFileInput("usuarios");
    		BufferedInputStream bis=new BufferedInputStream(fis);
    		StringBuffer b = new StringBuffer();
    		while(bis.available()!=0){
    			char c=(char)bis.read();
    			b.append(c);
    		}
    		bis.close();
    		fis.close();*/

		//DB
		String usuarioL=UIHelper.getText(this, R.id.editTextUserLogin);
		String passL=UIHelper.getText(this, R.id.editTextPasswordLogin);

		datasource = new UsersDataSource(this);
		datasource.open();
		Cursor cursor= datasource.getDatabase().query(UsersDBOpenHelper.TABLE_USERS, allColumns, null, null, null, null, null);
		Log.i(LOGTAG,"Returned " + cursor.getCount() + " rows");
		String userActual;
		String passActual;
		if(cursor.getCount()>0){
			while(cursor.moveToNext()){
				id=cursor.getLong(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_ID));
				userActual=cursor.getString(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_USER));
				passActual=cursor.getString(cursor.getColumnIndex(UsersDBOpenHelper.COLUMN_PASSWORD));
				if(usuarioL.equals(userActual) && passL.equals(passActual)){
					status=true;
					iniciarSesion(v);
					break;
				}
			}
			datasource.close();
			if(!status){
				Toast.makeText(getApplicationContext(), "Usuario o contraseņa incorrectos", Toast.LENGTH_LONG).show();
			}
		}
		else{
			Toast.makeText(getApplicationContext(), "Usuario o contraseņa incorrectos", Toast.LENGTH_LONG).show();
		}
		/*JSONArray datos= new JSONArray(b.toString());
    		StringBuffer usuariosBuffer=new StringBuffer();

    	for(int i=0;i<datos.length();i++){
    			String user=datos.getJSONObject(i).getString("Usuario");
    			usuariosBuffer.append(user+"\n");
    			String pass=datos.getJSONObject(i).getString("Password");
    			usuariosBuffer.append(pass+"\n");

    			if(user.equals(usuarioL) && pass.equals(passL)){
    				mp1.stop();
    				Intent intent = new Intent(this, MapaActivity.class);
    		    	startActivity(intent);
    			}
    			else{
    				Toast.makeText(getApplicationContext(), "Usuario o contraseņa incorrectos", Toast.LENGTH_LONG).show();	
    			}
    		}*/

		//UIHelper.displayText(this, R.id.textViewUsuario, usuariosBuffer.toString());
		/*}
	else{
		Log.d("LoginActivity", "No existe el archivo");
		Toast.makeText(getApplicationContext(), "Usuario o contraseņa incorrectos", Toast.LENGTH_LONG).show();	

	}*/

	}
	public void iniciarSesion(View v){
		mp1.stop();
		Intent intent = new Intent(this, MapaActivity.class);
		intent.putExtra("idUsuario",id);
		startActivity(intent);
	}
	//Te lleva a CrearCuenta    
	public void goToCrearCuenta(View v){
		mp1.stop();
		Intent intent = new Intent(this, CrearCuentaActivity.class);
		startActivity(intent);

	}


}
